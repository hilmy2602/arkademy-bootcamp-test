import java.io.*;

public class number3 {

    public static void main(String args[]) {
        rectangle(11);
    }

    static void rectangle(int m) {
        if (m % 2 == 0) {
            System.out.println("Panjang sisi harus ganjil");
        } else {
            int i, j;
            for (i = 1; i <= m; i++) {
                for (j = 1; j <= m; j++) {
                    if (i == 1 || i == m ||
                            j == 1 || j == m)
                        System.out.print("&");
                    else
                        System.out.print("=");
                }
                System.out.println();
            }
        }
    }
} 