<?php
class Biodata {

	public $_json;
	public $_name;
	public $_address;
	public $_hobbies;
	public $_is_married;
	public $_skills;
	public $_school;

	public function getData() {
		$this->_json = array(
			'name'			=> $this->_name,
			'address'		=> $this->_address,
			'hobbies'		=> $this->_hobbies,
			'is_married'	=> $this->_is_married,
			'skills'		=> $this->_skills,
			'school'		=> (object) $this->_school,
		);
		return json_encode($this->_json);
	}
}

$_bio 				= new Biodata();
$_bio->_name 		= 'Muhammad Hilmy Fauzi';
$_bio->_address 	= 'Ruko Villa Hang Lekir DD2/7 Legenda Malaka';
$_bio->_hobbies		= array( 'exploring', 'reading', 'coding' );
$_bio->_is_married	= false;
$_bio->_school		= array( 'high_school'	=> 'SMK IDN Boarding School' );
$_bio->_skills		= array( 'Android Mobile Development' , 'UI/UX Design' );

echo $_bio->getData();

?>