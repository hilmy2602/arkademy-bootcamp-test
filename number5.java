public class number5 {

    public static void main(String[] args) {
        hitungParkir(2);
    }

    private static void hitungParkir(int s) {
    	int biaya = 2000;
    	int hasil;
        if (s <= 2) {
			hasil = biaya * s;
			System.out.println("Biaya: " + Integer.toString(hasil));
        } else if (s > 2 && s <= 8) {
        	hasil = 4000 + (s - 2) * 1000;
        	System.out.println("Biaya: " + Integer.toString(hasil));
        } else {
        	System.out.println("Biaya: 10000");
        }
    }
}